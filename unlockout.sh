#/bin/bash

DEFAULT_TOMCAT_HOME=/opt/tomcat
DEFAULT_OPENDJ_HOME=/opt/opendj
DEFAULT_GLUU_PKI_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SCRIPT_ID='!A5DC.8175'

# helper functions
. lib/common.sh

### begin script ###
init_gluu_config

# run the unlockout script
gluu_unlockout

echo
echo 'Gluu authentication mode reset to default.'
