#/bin/bash

DEFAULT_TOMCAT_HOME=/opt/tomcat
DEFAULT_OPENDJ_HOME=/opt/opendj
DEFAULT_GLUU_PKI_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SCRIPT_ID='!A5DC.8175'

# helper functions
. "$DEFAULT_GLUU_PKI_HOME/lib/common.sh"

### begin script ###
init_gluu_config

# get the gluuOrganization inum
GLUU_ORG="$(get_ldap_attribute '(objectClass=gluuOrganization)' o)"

if [ -z "$GLUU_ORG" ]; then
	echo "Unable to find Gluu organization from LDAP"
	exit 1
fi

# get the base entry for ou=scripts
SCRIPTS_BASE="$(get_ldap_attribute '(&(objectClass=organizationalUnit)(ou=scripts))' dn)"

if [ -z "$SCRIPTS_BASE" ]; then
	echo "Unable to find Gluu Custom scripts LDAP base"
	exit 1
fi
SCRIPTS_BASE="$(echo $SCRIPTS_BASE |cut -f 2- -d :)"
if [ -z "$SCRIPTS_BASE" ]; then
	echo "Unable to parse Gluu Custom scripts LDAP base"
	exit 1
fi

# insert an oxCustomScript 
# attempt to insert the script (if it does not already exist)
"$LDAPMODIFY" "${LDAP_OPTIONS[@]}" < <(cat "$GLUU_PKI_HOME/ldap/pki.ldap" | \
  sed -r -e 's/\$SCRIPT_ID/'"${SCRIPT_ID}"'/g' \
      -e 's!\$GLUU_PKI_HOME!'"${GLUU_PKI_HOME}"'!g' \
      -e 's/\$GLUU_ORG/'"${GLUU_ORG}"'/g' \
      -e 's/\$SCRIPTS_BASE/'"${SCRIPTS_BASE}"'/g')

# copy the files
SRC_FILES="$GLUU_PKI_HOME/tomcat/"
DEST_DIR="$TOMCAT_HOME/"
rsync -av --progress "$SRC_FILES" "$DEST_DIR"

# restart tomcat
echo -n 'Tomcat needs to be restarted. Restart now? [Y/n] '
read TOMCAT_RESTART

if [ -z "$TOMCAT_RESTART" ]; then
	TOMCAT_RESTART=y
elif [ "$TOMCAT_RESTART" = "n" ]; then
	TOMCAT_RESTART=
fi

if [ "$TOMCAT_RESTART" ]; then
	gluu_restart_wait
	echo 'gluu_pki installed! You can now enable the script in your gluu interface.'
else
	echo 'gluu_pki installed. You will need to restart tomcat to enable the pki script.'
fi
