This is a PKI authentication plugin for Gluu. Tested on version 2.4.2 and 2.4.4.

# Installation
The PKI authentication script requires cooperation from both the front webserver and the Gluu application server.

## Front Webserver
The front webserver should be used to validate and parse the client certificate and pass the data to the Gluu application server in the form of HTTP headers. The application server then uses the headers to read info from the client certificate and determine whether the front webserver validated it correctly.


### Apache
If you are using the default Gluu configuration, then your front webserver is apache running on the same machine.

The following lines should be added to your apache virtualhost entry for Gluu.

~~~
# enable SSL client certificates
SSLVerifyClient optional_no_ca
SSLVerifyDepth 4
SSLCACertificateFile /etc/certs/root-ca.crt
SSLCARevocationFile /etc/certs/root-ca.crl
# uncomment for apache 2.3 and later to enable OCSP stapling
# SSLUseStapling On
# SSLStaplingCache "shmcb:logs/ssl_stapling(32768)"
RequestHeader set SSL_CLIENT_CERT %{SSL_CLIENT_CERT}s
RequestHeader set SSL_CLIENT_VERIFY %{SSL_CLIENT_VERIFY}s
RequestHeader set SSL_CLIENT_S_DN %{SSL_CLIENT_S_DN}s
~~~

SSLCACertificateFile should point to the path containing the CA Certificate used for client verification.

SSLVerifyDepth specifies how deeply the webserver should verify before deciding the client certificate is valid. The number used here will depend on how far away the client certificates are from the trusted CA certificate.

SSLCARevocationFile should point to the path containing the certificate revocation list for your Client CA. If you are using CRLs, you should download the CRL from the client CA to that file. Otherwise, comment out the line.

Apache 2.3 and higher support OCSP stapling, which allows the webserver to verify client certificates in real-time. If you have apache 2.3 and higher, uncomment the SSL stapling parameters.

After adding the above, restart the webserver.

~~~
service httpd restart
~~~

### nginx
You may also use nginx as your front web server.

The following lines should be added to your server section for Gluu.

~~~
ssl_client_certificate /etc/certs/root-ca.crt;
ssl_crl /etc/certs/root-ca.crl;
ssl_verify_client optional_no_ca;
ssl_verify_depth 4;
# ssl_stapling on;
# ssl_stapling_verify on;
# resolver 8.8.8.8;
~~~

ssl_client_certificate should point to the path containing a concatenated CA Certificates in PEM format used for client verification. The list of certificates will be sent to clients.

ssl_verify_depth specifies how deeply the webserver should verify before deciding the client certificate is valid. The number used here will depend on how far away the client certificates are from the trusted CA certificate.

ssl_crl should point to the path containing the certificate revocation list for your Client CA. If you are using CRLs, you should download the CRL from the client CA to that file. Otherwise, comment out the line.

You may choose to enable OCSP stapling, which allows the webserver to verify client certificates in real-time. If so, uncomment the ssl_stapling and resolver lines. For OCSP to work, the resolver line should point to a valid DNS server.

In addition, the nginx location or server sections that implement reverse proxy should set the following HTTP headers in the proxy request:
~~~
proxy_set_header SSL_CLIENT_CERT $ssl_client_cert;
proxy_set_header SSL_CLIENT_S_DN $ssl_client_s_dn;
proxy_set_header SSL_CLIENT_VERIFY $ssl_client_verify;
~~~

The above can be added to /etc/nginx/proxy_params and included in the relevant nginx server section(s) that performs reverse proxy.
~~~
upstream oxauth {
	server GLUU-SERVER-BACKEND;
}

...
	location / {
		proxy_pass http://oxauth;
		proxy_redirect http://GLUU-DOMAIN-NAME https://GLUU-DOMAIN-NAME;
		include proxy_params;
	}
~~~

GLUU-SERVER-BACKEND is the IP or domain name of your Gluu tomcat instance.

GLUU-DOMAIN-NAME is the public domain name of your nginx server, for example, sso.gov.ph.


### PKI trust chain
The front webserver is responsible for validating and trusting the client certificate. The server should be configured to trust the client root authority at the operating system level.


### CRL file
The CRL file, or "certificate revocation list", is updated by the CA at regular intervals. The webserver should keep this file updated by redownloading it at regular intervals. How often to download the CRL depends on how often the CA updates it.

The following entry in /etc/crontab downloads CRL files at midnight everyday.

~~~
0 0 * * * wget -q http://url-of-crl-file /etc/certs/root-ca.crl
~~~



## Gluu Tomcat server
### Production Install
Checkout this repository into a folder inside the Gluu chroot, for example, `/opt/gluu_pki`.

~~~
sudo service gluu-server-2.4.2 login
git clone https://bitbucket.org/intelimina/gluu_pki
~~~

From inside the chroot, run the install-tomcat.sh script
~~~
$ cd /opt/gluu_pki
$ ./install-tomcat.sh
Gluu PKI installation directory? [/opt/gluu_pki]: 
Tomcat installation directory? [/opt/tomcat]: 
OpenDJ installation directory? [/opt/opendj]: 
...
Tomcat needs to be restarted. Restart now? [Y/n] 
Welcome to the Gluu Server!
Stopping Tomcat Servlet Container...
Stopped Tomcat Servlet Container.
Starting Tomcat Servlet Container...
Waiting for Tomcat Servlet Container......
running: PID:13418
Waiting for server to be ready................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................done.
gluu_pki installed! You can now enable the script in your gluu interface.
~~~

At this point skip to the "Gluu Interface" step.

### Manual Install
This section describes what the install-tomcat.sh script does. You may safely skip to the "Gluu Interface" step if you have already run the installer above.

The gluu_pki install script does the following to your gluu server.

1. copies the contents of the `tomcat/` directory to your Gluu tomcat.
2. creates a OxCustomScript object that points to the gluu_pki script.

#### Copying files
The `tomcat` directory contains files that need to be dropped into the oxauth webapp.

~~~
tomcat
└── webapps
    └── oxauth
        └── auth
            └── pki
                ├── basic-login.page.xml
                ├── basic-login.xhtml
                ├── cert-invalid.page.xmml
                ├── cert-invalid.xhtml
                ├── cert-nonmatching.page.xml
                ├── cert-nonmatching.xhtml
                ├── cert-not-selected.page.xml
                ├── cert-not-selected.xhtml
                ├── cert-unrecognized.page.xml
                └── cert-unrecognized.xhtml
~~~

Tomcat needs to be restarted for the files to be visible.

~~~
service tomcat restart
~~~

Gluu typically takes 1-2 minutes to be fully available again.

#### Adding the script
After copying the files and restarting Gluu, login as an admin user. Then navigate to Configuration -> Manage Custom Scripts -> Person Authentication.

From here create a new script with the following properties:

- Name: pki
- Description: authentication script using PKI
- Programming Language: Python
- Location type: File
- Usage type: both methods
- Script path: /opt/gluu_pki/gluu_pki.py
- Enabled: check

And click 'Update'

### Gluu interface
#### Person Authentication script
The PKI script can be enabled / disabled from the Gluu interface. If enabled, the PKI authentication will appear in the list of "Authentication methods" below.

From the Gluu admin, navigate to Configuration -> Manage Custom Scripts -> Person Authentication. A "PKI" script should be visible there and can be enabled / disabled as needed.

#### Default Authentication Method
The default authentication method controls how applications login to Gluu by default.

From the Gluu admin, navigate to Configuration -> Manage Authentication -> Default Authentication Method. If the PKI person authentication script is enabled, it should appear in the dropdown boxes of the authentication methods here.

"Authentication mode" controls how OpenID Connect client applications login to Gluu. Set it to pki to make client applications use PKI authentication by default. Set it to internal to use the default LDAP-based authentication.

"oxTrust Authentication Mode" controls how users can login to the Gluu interface itself. Set it to pki to make the admin interface (oxTrust) use PKI authentication by default. Set it to internal to use the default LDAP-based authentication.

# Debugging and Lockout
If the PKI script is improperly configured, or you do not have a working PKI trust chain on the webserver, it is possible to get locked out of your Gluu server.

If this happens, you can use the `unlockout.sh` script to return the oxTrust default authentication method back to the default username / password -based login.

~~~
$ ./unlockout.sh
Gluu PKI installation directory? [/opt/gluu_pki]: 
Tomcat installation directory? [/opt/tomcat]: 
OpenDJ installation directory? [/opt/opendj]: 

Gluu authentication mode reset to default.
~~~
