#/bin/bash

DEFAULT_TOMCAT_HOME=/opt/tomcat
DEFAULT_OPENDJ_HOME=/opt/opendj
DEFAULT_GLUU_PKI_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SCRIPT_ID='!A5DC.8175'

# helper functions
. "$DEFAULT_GLUU_PKI_HOME/lib/common.sh"

### begin script ###
init_gluu_config

# find the DN of the matching oxCustomScript
SCRIPT_DN="$(get_ldap_attribute '(&(objectClass=oxCustomScript)(oxModuleProperty=*"value2": "'"$GLUU_PKI_HOME/gluu_pki.py"'"*))' dn)"

if [ "$SCRIPT_DN" ]; then
	# ensure that the script isnt being used for authentication
	SCRIPT_NAME="$(get_ldap_attribute '(&(objectClass=oxCustomScript)(oxModuleProperty=*"value2": "'"$GLUU_PKI_HOME/gluu_pki.py"'"*))' displayName)"

	OX_AUTH_MODE="$(get_ldap_attribute '(objectClass=gluuAppliance)' oxAuthenticationMode)"
	OXTRUST_AUTH_MODE="$(get_ldap_attribute '(objectClass=gluuAppliance)' oxAuthenticationMode)"
	if [ "$OX_AUTH_MODE" = "$SCRIPT_NAME" ] || [ "$OXTRUST_AUTH_MODE" = "$SCRIPT_NAME" ]; then
		echo "Gluu authentication was set to $SCRIPT_NAME. Resetting to default. "
		gluu_unlockout
	fi

	# attempt to delete the script
	"$LDAPDELETE" "${LDAP_OPTIONS[@]}" "$SCRIPT_DN"
else
	echo "Unable to find PKI script in LDAP"
fi

# delete the files
DEST_DIR="$TOMCAT_HOME/webapps/oxauth/auth/pki"
rm -Rvf "$DEST_DIR"

# restart tomcat
echo -n 'Tomcat needs to be restarted. Restart now? [Y/n] '
read TOMCAT_RESTART

if [ -z "$TOMCAT_RESTART" ]; then
	TOMCAT_RESTART=y
elif [ "$TOMCAT_RESTART" = "n" ]; then
	TOMCAT_RESTART=
fi

if [ "$TOMCAT_RESTART" ]; then
	gluu_restart_wait
	echo 'gluu_pki uninstalled!'
else
	echo 'gluu_pki uninstalled. You will need to restart tomcat.'
fi
