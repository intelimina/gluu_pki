# helper functions

# given a properties file and a key, returns the value
get_key() {
	FILENAME=$1
	KEY=$2

	# .properties files have 3 formats per line. dont blame me
	LINE="$(cat $FILENAME |egrep "^$KEY\s*[:=\s]?")"
	if [ "$(echo $LINE|egrep "^$KEY\s*:")" ]; then
		# key = value
		VALUE="$(echo $LINE|cut -f 2- -d :| sed 's/^\s*//g')"
	elif [ "$(echo $LINE|egrep "^$KEY\s*=")" ]; then
		# key : value
		VALUE="$(echo $LINE|cut -f 2- -d =| sed 's/^\s*//g')"
	elif [ "$(echo $LINE|egrep "^$KEY\s")" ]; then
		# key value
		VALUE="$(echo $LINE|cut -f 2- -d ' '| sed 's/^\s*//g')"
	fi

	echo $VALUE
}

# given a filter, grabs an ldap attribute from the filter
# uses the $LDAP___ connection variables in an ldap search
get_ldap_attribute() {
	FILTER=$1
	ATTRIBUTE=$2

	LDAP_OPTIONS[9]="--dontWrap"

	"$LDAPSEARCH" "${LDAP_OPTIONS[@]}" --baseDN="$LDAP_BASE" "$FILTER" "$ATTRIBUTE" | \
	    egrep '^'"$ATTRIBUTE": | \
	    cut -f 2- -d : | \
	    sed -r 's/^\s*//g'
}

# populates environment variables for tomcat etc
init_gluu_config() {
	# find installation dir
	echo -n 'Gluu PKI installation directory? ['"$DEFAULT_GLUU_PKI_HOME"']: '
	read GLUU_PKI_HOME

	if [ -z "$GLUU_PKI_HOME" ]; then
		GLUU_PKI_HOME="$DEFAULT_GLUU_PKI_HOME"
	fi

	if ! [ -d "$GLUU_PKI_HOME" ]; then
		echo "Unable to open $GLUU_PKI_HOME"
		exit 1
	fi

	# find tomcat dir
	echo -n 'Tomcat installation directory? ['"$DEFAULT_TOMCAT_HOME"']: '
	read TOMCAT_HOME

	if [ -z "$TOMCAT_HOME" ]; then
		TOMCAT_HOME="$DEFAULT_TOMCAT_HOME"
	fi

	if ! [ -d "$TOMCAT_HOME" ]; then
		echo "Unable to open $TOMCAT_HOME"
		exit 1
	fi

	# get LDAP configuration
	OXLDAPCONF="$TOMCAT_HOME/conf/ox-ldap.properties"
	if ! [ -f "$OXLDAPCONF" ]; then
		OXLDAPCONF="$TOMCAT_HOME/conf/oxauth-ldap.properties"
	fi

	if ! [ -f "$OXLDAPCONF" ]; then
		echo "Unable to find Gluu LDAP config."
		exit 1
	fi

	# get username and password
	LDAP_USER="$(get_key $OXLDAPCONF bindDN)"
	if [ -z "$LDAP_USER" ]; then
		echo "Unable to parse Gluu LDAP user from $OXLDAPCONF"
		exit 1
	fi

	LDAP_PASS="$(get_key $OXLDAPCONF bindPassword)"
	if [ -z "$LDAP_PASS" ]; then
		echo "Unable to parse Gluu LDAP pass from $OXLDAPCONF"
		exit 1
	fi

	# decode LDAP pass
	export TOMCAT_HOME
	LDAP_PASS="$($GLUU_PKI_HOME/encode.py -D $LDAP_PASS)"
	if [ -z "$LDAP_PASS" ]; then
	        echo "Unable to decode LDAP pass from $OXLDAPCONF"
	        exit 1
	fi

	# server (host:port)
	LDAP_SERVER="$(get_key $OXLDAPCONF servers)"
	if [ -z "$LDAP_SERVER" ]; then
	        echo "Unable to decode LDAP server from $OXLDAPCONF"
	        exit 1
	fi

	# separate hostname from port
	LDAP_HOST="$(echo $LDAP_SERVER| cut -f 1 -d :)"
	if [ -z "$LDAP_HOST" ]; then
	        echo "Unable to determine LDAP host from $LDAP_SERVER"
	        exit 1
	fi
	LDAP_PORT="$(echo $LDAP_SERVER| cut -f 2 -d :)"
	if [ -z "$LDAP_PORT" ]; then
	        echo "Unable to determine LDAP port from $LDAP_SERVER"
	        exit 1
	fi

	# get SSL config
	LDAP_SSL="$(get_key $OXLDAPCONF useSSL)"
	if [ "$LDAP_SSL" = true ] || \
	   [ "$LDAP_SSL" = 1 ] || \
	   [ "$LDAP_SSL" = "yes" ]; then
	        LDAP_SSL=1
	else
	        LDAP_SSL=
	fi

	# get baseDN
	LDAP_BASE="$(get_key $OXLDAPCONF baseDNs)"
	if [ -z "$LDAP_BASE" ]; then
	        LDAP_BASE="o=gluu"
	fi
	
	# find opendj dir
	echo -n 'OpenDJ installation directory? ['"$DEFAULT_OPENDJ_HOME"']: '
	read OPENDJ_HOME
	if [ -z "$OPENDJ_HOME" ]; then
	        OPENDJ_HOME="$DEFAULT_OPENDJ_HOME"
	fi
	
	if ! [ -d "$OPENDJ_HOME" ]; then
	        echo "Unable to find $OPENDJ_HOME"
	        exit 1
	fi
	
	###
	# set LDAP options
	LDAP_OPTIONS[0]="--hostname=$LDAP_HOST"
	LDAP_OPTIONS[1]="--port=$LDAP_PORT"
	if [ "$LDAP_SSL" ]; then
	        LDAP_OPTIONS[3]="--useSSL"
	fi
	LDAP_OPTIONS[4]="--bindDN=$LDAP_USER"
	LDAP_OPTIONS[5]="--bindPassword=$LDAP_PASS"

	# get ldapsearch
	LDAPSEARCH="$OPENDJ_HOME/bin/ldapsearch"
	if ! [ -x "$LDAPSEARCH" ]; then
		echo "ldapspearch \(${LDAPSEARCH}\) not executable"
		exit 1
	fi

	# get ldapmodify
	LDAPMODIFY="$OPENDJ_HOME/bin/ldapmodify"
	if ! [ -x "$LDAPMODIFY" ]; then
		echo "ldapmodify (${LDAPMODIFY}) not executable"
		exit 1
	fi

	# get ldapdelete
	LDAPDELETE="$OPENDJ_HOME/bin/ldapdelete"
	if ! [ -x "$LDAPDELETE" ]; then
		echo "ldapdelete not executable ${LDAPDELETE}"
		exit 1
	fi
}

gluu_unlockout() {
	# get the gluuAppliance entry
	GLUU_APPLIANCE_DN="$(get_ldap_attribute '(objectClass=gluuAppliance)' dn)"
	
	# get oxAuthenticationMode
	OX_AUTH_MODE="$(get_ldap_attribute '(objectClass=gluuAppliance)' oxAuthenticationMode)"
	
	# get oxTrustAuthenticationMode
	OXTRUST_AUTH_MODE="$(get_ldap_attribute '(objectClass=gluuAppliance)' oxTrustAuthenticationMode)"
	
	if [ -z "$GLUU_APPLIANCE_DN" ]; then
		echo "Unable to find Gluu Appliance entry"
		exit 1
	fi
	GLUU_APPLIANCE_DN="$(echo $GLUU_APPLIANCE_DN |cut -f 2- -d :)"
	if [ -z "$GLUU_APPLIANCE_DN" ]; then
		echo "Unable to parse Gluu Appliance entry"
		exit 1
	fi
	
	# delete the authentication attributes
	if ! [ -z "$OX_AUTH_MODE" ]; then
		echo "Resetting ox authentication mode to default"
		"$LDAPMODIFY" "${LDAP_OPTIONS[@]}" < <(cat "$GLUU_PKI_HOME/ldap/unlockout.ldap" | \
		  sed -r -e 's/\$GLUU_APPLIANCE_DN/'"${GLUU_APPLIANCE_DN}"'/g' \
		      -e 's!\$OX_AUTH_ATTR!oxAuthenticationMode!g' \
		      -e 's!\$OX_AUTH_MODE!'"${OX_AUTH_MODE}"'!g'
		  )
	fi
	
	# delete the authentication attributes
	if ! [ -z "$OXTRUST_AUTH_MODE" ]; then
		echo "Resetting oxTrust authentication mode to default"
		"$LDAPMODIFY" "${LDAP_OPTIONS[@]}" < <(cat "$GLUU_PKI_HOME/ldap/unlockout.ldap" | \
		  sed -r -e 's/\$GLUU_APPLIANCE_DN/'"${GLUU_APPLIANCE_DN}"'/g' \
		      -e 's!\$OX_AUTH_ATTR!oxTrustAuthenticationMode!g' \
		      -e 's!\$OX_AUTH_MODE!'"${OXTRUST_AUTH_MODE}"'!g'
		  )
	fi
}

# restarts tomcat and waits for it to be ready
gluu_restart_wait() {
	service tomcat restart
	echo -ne "Waiting for server to be ready"
	tail -n0 -F "$TOMCAT_HOME/logs/wrapper.log" 2>&1 | \
	while read LINE; do
		echo -ne .
		echo $LINE | egrep -q 'Server startup in [0-9]* ms' && pkill -P $$ tail
	done
	echo done.
}
