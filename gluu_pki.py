from org.xdi.model.custom.script.type.auth import PersonAuthenticationType
from org.jboss.seam.security import Identity
from org.jboss.seam.contexts import Context, Contexts
from javax.faces.context import FacesContext
from org.xdi.oxauth.service import UserService
from org.xdi.util import StringHelper
from org.xdi.oxauth.util import ServerUtil
from java.util import Arrays

import base64
import java
import re
import os
import subprocess
from subprocess import Popen, PIPE
import StringIO

class PersonAuthentication(PersonAuthenticationType):
    def __init__(self, currentTimeMillis):
        self.currentTimeMillis = currentTimeMillis

    def init(self, configurationAttributes):
        os.system("echo $(date --rfc-3339=seconds) Cert. Basic. Initialization >> /tmp/pki.log")
        os.system("echo $(date --rfc-3339=seconds) Cert. Initialized successfully >> /tmp/pki.log")
        return True

    def destroy(self, configurationAttributes):
        os.system("echo $(date --rfc-3339=seconds) Cert. Destroy >> /tmp/pki.log")
        os.system("echo $(date --rfc-3339=seconds) Cert. Destroyed successfully >> /tmp/pki.log")
        return True

    def getApiVersion(self):
        return 1

    def isValidAuthenticationMethod(self, usageType, configurationAttributes):
        return True

    def getAlternativeAuthenticationMethod(self, usageType, configurationAttributes):
        return 'internal'
        return None

    def authenticate(self, configurationAttributes, requestParameters, step):
        context = Contexts.getEventContext()
        os.system("echo $(date --rfc-3339=seconds) Cert. Begin auth for step 1 >> /tmp/pki.log")
        userService = UserService.instance()
        request = FacesContext.getCurrentInstance().getExternalContext().getRequest()

        if (step == 1):
            os.system("echo $(date --rfc-3339=seconds) Cert. step1: Basic auth >> /tmp/pki.log")

            credentials = Identity.instance().getCredentials()
            oxauth_user = self.processBasicAuthentication(credentials)
            if oxauth_user == None:
                return False

            cert_user = self.processSSLAuthentication()
            if cert_user == None:
                return True # proceed to next step and show error message
            
            oxauth_user_id = oxauth_user.getUserId()
            cert_user_id = cert_user.getUserId()

            if cert_user_id == oxauth_user_id:
                context.set("cert_error", False)
                context.set("cert_matching", True)
                os.system("echo $(date --rfc-3339=seconds) Cert. user match: oxauth_user matches cert_user: %s. Logged in. >> /tmp/pki.log" % (oxauth_user_id))
                return userService.authenticate(oxauth_user_id)

            os.system("echo $(date --rfc-3339=seconds) Cert. Authenticate step2: oxauth_user does not match cert_user: %s vs %s.>> /tmp/pki.log" % (oxauth_user_id, cert_user_id))
            context.set("cert_error", True)
            context.set("cert_matching", False)
            return True # proceed to next step and show error message
        else:
            return False


    def prepareForStep(self, configurationAttributes, requestParameters, step):
        os.system("echo $(date --rfc-3339=seconds) Cert. Prepare for step %d >> /tmp/pki.log" % step)
        context = Contexts.getEventContext()
        request = FacesContext.getCurrentInstance().getExternalContext().getRequest()
        credentials = Identity.instance().getCredentials()
        userService = UserService.instance()

        ssl_client_cert = self.getCertFromHeader()
        ssl_client_email = self.getEmailFromCert(ssl_client_cert)
        ssl_client_verify = request.getHeader('SSL_CLIENT_VERIFY')
        ssl_client_dn = request.getHeader('SSL_CLIENT_S_DN')

        # check if user exists
        if ssl_client_email:
            user = userService.getUserByAttribute('mail', ssl_client_email)
            if user:
                credentials.setUsername(user.getUserId())

        context.set('ssl_client_cert', ssl_client_cert)
        context.set('ssl_client_email', ssl_client_email)
        context.set('ssl_client_verify', ssl_client_verify)
        context.set('ssl_client_dn', ssl_client_dn)

        return True

    def getExtraParametersForStep(self, configurationAttributes, step):
        return Arrays.asList(
            "oxauth_user",
            "cert_error",
            "cert_selected",
            "cert_valid",
            "cert_recognized",
            "cert_matching",
            "ssl_client_cert",
            "ssl_client_verify",
            "ssl_client_s_dn",
            "ssl_client_email"
       )

    def getCountAuthenticationSteps(self, configurationAttributes):
        cert_error = self.getSessionAttribute("cert_error")
        if cert_error == True:
            return 2
        return 1

    def getPageForStep(self, configurationAttributes, step):
        if (step == 1):
            return "/auth/pki/basic-login.xhtml"
        elif (step == 2): # error pages
            cert_selected = self.getSessionAttribute('cert_selected')
            if True != cert_selected:
                return "/auth/pki/cert-not-selected.xhtml"

            cert_valid = self.getSessionAttribute('cert_valid')
            if True != cert_valid:
                return "/auth/pki/cert-invalid.xhtml"

            cert_recognized = self.getSessionAttribute('cert_recognized')
            if True != cert_recognized:
                return "/auth/pki/cert-unrecognized.xhtml"

            cert_matching = self.getSessionAttribute('cert_matching')
            if True != cert_matching:
                return "/auth/pki/cert-nonmatching.xhtml"

            return "/error.xhtml"
                
        return ""

    def getSessionAttribute(self, attribute_name):
        context = Contexts.getEventContext()
        
        # Try to get attribute name from Seam event context
        if context.isSet(attribute_name):
            return context.get(attribute_name)

        # Try to get attribute from persistent session
        session_attributes = context.get("sessionAttributes")
        if session_attributes.containsKey(attribute_name):
            return session_attributes.get(attribute_name)
        
        return None

    def getCertFromHeader(self):
        request = FacesContext.getCurrentInstance().getExternalContext().getRequest()
	ssl_client_cert = request.getHeader('SSL_CLIENT_CERT')

        if ssl_client_cert == None or len(ssl_client_cert) == 0:
            return None

        # reformat ssl_client_cert
        ssl_client_cert = re.sub(r"-----BEGIN CERTIFICATE-----", '-----BEGIN CERTIFICATE-----\n', ssl_client_cert)
        ssl_client_cert = re.sub(r"-----END CERTIFICATE-----", '\n-----END CERTIFICATE-----', ssl_client_cert)
        ssl_client_cert = re.sub(r'(.{64})', "\\1\n", ssl_client_cert)
        return ssl_client_cert

    def getEmailFromCert(self, cert):
        p = Popen(["openssl", "x509", "-noout", "-email"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate(cert)
        email = stdout.rstrip()
        if len(email) == 0:
            return None
        
        return email

    def processBasicAuthentication(self, credentials):
        userService = UserService.instance()

        username = credentials.getUsername()
        password = credentials.getPassword()
        os.system("echo $(dat --rfc-3339=seconds) Cert. Basic Auth: user: %s >> /tmp/pki.log" % username)

        logged_in = False
        if (StringHelper.isNotEmptyString(username) and StringHelper.isNotEmptyString(password)):
            logged_in = userService.authenticate(username, password)

        if (not logged_in):
            return None

        find_user_by_uid = userService.getUser(username)
        if (find_user_by_uid == None):
            os.system("echo $(date --rfc-3339=seconds) Cert. Process basic authentication. Failed to find user by username '%s' >> /tmp/pki.log" % user_name)
        
        return find_user_by_uid

    def processSSLAuthentication(self):
        context = Contexts.getEventContext()
        os.system("echo $(date --rfc-3339=seconds) Cert. SSL auth >> /tmp/pki.log")
        userService = UserService.instance()
        request = FacesContext.getCurrentInstance().getExternalContext().getRequest()

        ssl_client_cert = self.getSessionAttribute('ssl_client_cert')
        ssl_client_verify = self.getSessionAttribute('ssl_client_verify')
        ssl_client_dn = self.getSessionAttribute('ssl_client_dn')
        
        os.system("echo $(date --rfc-3339=seconds) Cert. Client verify: %s, dn: %s, cert: %s >> /tmp/pki.log" % (ssl_client_verify, ssl_client_dn, ssl_client_cert) )
            
        # Validate if user selected certificate
        if ssl_client_verify == "NONE":
            os.system("echo $(date --rfc-3339=seconds) Cert. Verify: User not selected any certs >> /tmp/pki.log")
            context.set("cert_error", True)
            context.set("cert_selected", False)
                    
            return None
        context.set("cert_selected", True)

        if ssl_client_verify != "SUCCESS":
            os.system("echo $(date --rfc-3339-seconds) Cert. Verify: Certificate invalid (%s) >> /tmp/pki.log" % ssl_client_verify)
            context.set("cert_error", True)
            context.set('cert_valid', False)

            return None
        context.set('cert_valid', True)

        email = self.getSessionAttribute('ssl_client_email')
        if email == None:
            os.system("echo $(date --rfc-3339=seconds) Cert. Contents: email not found >> /tmp/pki.log")
            context.set("cert_error", True)
            context.set('cert_recognized', False)
            return None

        os.system("echo $(date --rfc-3339=seconds) Cert. Contents: email = %s >> /tmp/pki.log" % email)
        cert_user = userService.getUserByAttribute('mail', email)
        if cert_user == None:
            os.system("echo $(date --rfc-3339=seconds) Cert. User: user not found >> /tmp/pki.log")
            context.set("cert_error", True)
            context.set('cert_recognized', False)
            return None
        context.set('cert_recognized', True)

        os.system("echo $(date --rfc-3339=seconds) Cert. User: cert_user found: %s >> /tmp/pki.log" % cert_user)
        return cert_user

    def logout(self, configurationAttributes, requestParameters):
        return True
